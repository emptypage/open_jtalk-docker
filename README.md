# emptypage/open_jtalk

Ubuntu with Open JTalk preinstalled.

- [Project Home Page (Bitbucket)](https://bitbucket.org/emptypage/open_jtalk-docker/src/master/)
- [Docker Hub Repository page](https://hub.docker.com/r/emptypage/open_jtalk)

It contains ready-to-use `open_jtalk` and `hts_engine` commands.

## Dictionary and Voice Data

The dictionary files are contained in `/usr/local/lib/open_jtalk/dic`, and
`*.htsvoice` voice data files are in subdirectories under
`/usr/local/lib/open_jtalk/voice`.

## Links

- [Open JTalk](http://open-jtalk.sourceforge.net)
- [HTS Engine](http://hts-engine.sourceforge.net)
- [emptypage/open_jtalk-builder](https://hub.docker.com/r/emptypage/open_jtalk-builder)
