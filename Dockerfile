FROM emptypage/open_jtalk-builder:1.11 as builder
FROM ubuntu:20.04
COPY --from=builder /root/open_jtalk-builder.tar.gz /root/
RUN set -x && \
    tar zxvf /root/open_jtalk-builder.tar.gz && \
    rm /root/open_jtalk-builder.tar.gz
CMD open_jtalk
